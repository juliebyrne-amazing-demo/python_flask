# base image
FROM python:3.8

LABEL MAINTAINER="James Sandlin <jsandlin@gitlab.com>"

ENV PACKAGES="\
    python3 \
    bash \
    curl \
    bzip2 \
    gcc \
    util-linux \
"
RUN set -ex && mkdir -p /usr/src/app

WORKDIR /usr/src/app

RUN set -ex \
    && apt-get install --upgrade -y $PACKAGES \
    && apt-get upgrade -y \
    && python -m pip install --upgrade pip \
    && pip install pipenv --upgrade


# RUN apt-get remove perl -y

COPY . /usr/src/app

RUN  set -ex  \
    && pipenv --python 3.8 \
    && pipenv install --deploy --system --dev \
    && pip install coverage

## set working directory
WORKDIR /usr/src/app/demo

ENV PYTHONPATH=/usr/src/app

CMD ["python", "wsgi.py"]
