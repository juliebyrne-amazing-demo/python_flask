from flask import Flask
app = Flask(__name__)

@app.route('/v1')
def index():
    return 'v1'

@app.route('/v2')
def v2():
    return 'v2'

@app.route('/company')
def company():
    company = {'name': 'GitLab'}
    return render_template('index.htm', title='Just a Title', company=company)
